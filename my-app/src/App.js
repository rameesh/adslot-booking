
import React from 'react';
import './App.css';
import logo from './images/logo.png';
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import { MDBDataTable} from 'mdbreact';

const DatatablePage = () => {
  
  const data = {
    columns: [
      {
        label: 'ID',
        field: 'ID',
        sort: 'asc',
        width: 150
      },
      {
        label: 'Product name',
        field: 'product_name',
        sort: 'asc',
        width: 270
      },
      {
        label: 'Quantity',
        field: 'quantity',
        sort: 'asc',
        width: 200
      },
      {
        label: 'Rate',
        field: 'rate',
        sort: 'asc',
        width: 100
      },
      {
        label: 'Cost',
        field: 'cost',
        sort: 'asc',
        width: 150
      },
      
    ],
    rows: [
      {
        ID: 'B1254',
        product_name: 'Home page banner ad',
        quantity: '150000',
        rate: '$50.00',
        cost: '$7500.00',

      },
      {
        ID: '3055B',
        product_name: 'Run of site',
        quantity: '100000',
        rate: '$30.00',
        cost: '$3000.00',

      },
      {
        ID: 'A610E',
        product_name: 'ATF Network Runway',
        quantity: '10000',
        rate: '$70.00',
        cost: '$700.00',

      },
      
    ]
  };

  return (

    <div class="App container pt-5">
      <div class="App-header pb-5">
        <img src={logo} class="App-logo" alt="logo"></img>
      </div>
      <div class="row">
     
        <div class="col-md-12 ">
          <h2 class="text-left pb-4 border-bottom">Bookings</h2>
        </div>
      </div>

    {/*  <MDBDataTable data="https://YOUR_API_URL" /> */}

    
    <MDBDataTable
      // striped
      bordered
      hover
      data={data}
      paging={false}
      responsiveSm={true}
      sorting={false}
    />

      <div class="row ">
        <div class="col-md-12 ">
          <h2 class="text-left py-4">Automated Genarated</h2>
        </div>
      </div>


      <MDBDataTable
      // striped
      bordered
      hover
      data={data}
      paging={false}
      searching={false}
    />
    
  </div> 
   
  );
}

export default DatatablePage;
